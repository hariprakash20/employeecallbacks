let data = require('./data.json');
let fs = require('fs');
const {detailsOfEmployees, groupEmployeesOnCompany, detailsOfACompany, removeId, sortEmployees, swapCompanies, addBirthdays} = require('./employeeCallbackSolution.cjs');
let employeeData = data.employees

function employeeCallbacks(callback){
    detailsOfEmployees(employeeData, (err,result)=>{
        if(err) callback(err, null);
        else return groupEmployeesOnCompany(result,(err,result)=>{
            if(err) callback(err, null);
            else return detailsOfACompany((err,result)=>{
                if(err) callback(err, null);
                else return removeId((err,result)=>{
                    if(err) callback(err, null);
                    else return sortEmployees((err,result)=>{
                        if(err) callback(err, null);
                        else return swapCompanies((err,result)=>{
                            if(err) callback(err, null);
                            else return addBirthdays((err,result)=>{
                                console.log("success");
                                return result;
                            })
                        }) 
                    })
                })
            })
        }) 
    })
}

employeeCallbacks((err, result)=>{
    if(err) console.log(err);
    else console.log(result);
});




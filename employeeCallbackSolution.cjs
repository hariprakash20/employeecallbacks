let data = require('./data.json');
let fs = require('fs');
let employeeData = data.employees

function detailsOfEmployees(employeeData, callback){
    let dataIds = [2,13,23];
    const selectedEmployees = employeeData.filter(employee => dataIds.includes(employee.id))
    fs.writeFile('./output/selectedEmployees.json',JSON.stringify(selectedEmployees), err =>{
        if(err){
            callback(err, null);
            return 
        }
        else callback(null,employeeData);
    }) 
}

function groupEmployeesOnCompany(employeeData, callback){
    let companyGroup = { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []};
    employeeData.map(employee =>  companyGroup[employee.company].push(employee))
    fs.writeFile('./output/companyGroup.json',JSON.stringify(companyGroup),err =>{
        if(err){
            callback(err,null);
            return;
        }
        else{
            callback(null, employeeData);
        }
        
    })
}

function addBirthdays(callback){
    let employeeBirthdays = employeeData.map(employee=>{
        if(employee.id%2 === 0) return {...employee, birthday: new Date().toDateString().substring(4)}
        else return employee; 
    })
    fs.writeFile('./output/employeesBirthdays.json',JSON.stringify(employeeBirthdays),err=>{
        if(err){
            callback(err, null);
            return;
        }else{
            callback(null, "Execution of all callbacks are successful");
        }
    })
}

function swapCompanies(callback){
    let swapedData = employeeData;
    let emp92Index = swapedData.findIndex(employee => employee.id===92);
    let emp93Index = swapedData.findIndex(employee => employee.id===93);
    let temp = swapedData[emp92Index].company;
    swapedData[emp92Index].company = swapedData[emp93Index].company;
    swapedData[emp93Index].company = temp;
    fs.writeFile('./output/swapedData.json',JSON.stringify(swapedData),err=>{
         if(err){
            callback(err,null);
            return;
        }
        else{
            callback(null, employeeData);
        }
    })
}

function sortEmployees(callback){
    employeeData.sort((a,b)=>{
    if(a.company > b.company) return 1;
    else if(a.company < b.company) return -1;
    else if(a.id > b.id) return 1;
    else if(a.id < b.id) return -1;
    else return 0;
    })
    fs.writeFile('./output/sortedEmployees.json',JSON.stringify(employeeData),err =>{
        if(err){
            callback(err,null);
            return;
        }
        else{
            callback(null, employeeData);
        }
    })
}

function removeId(callback){
        let id2Removed = employeeData.filter(employee => employee.id !== 2)
        fs.writeFile('./output/id2Removed.json',JSON.stringify(id2Removed), err =>{
            if(err){
                callback(err,null);
                return;
            }
            else{
                callback(null, employeeData);
            }
        })
}


function detailsOfACompany(callback){
    let powerpuffBrigadeCompany = employeeData.filter(employee => employee.company==='Powerpuff Brigade');
    fs.writeFile('./output/powerpuffBrigadeCompany.json',JSON.stringify(powerpuffBrigadeCompany),err =>{
    if(err){
        callback(err,null);
        return;
        }
        else{
            callback(null, employeeData);
        }
    })
}

module.exports = {detailsOfEmployees, groupEmployeesOnCompany, detailsOfACompany, removeId, sortEmployees, swapCompanies, addBirthdays}